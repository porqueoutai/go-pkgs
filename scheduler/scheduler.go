// Copyright (c) 2019, prprprus All rights reserved.
// Use of this source code is governed by a BSD-style .
// license that can be found in the LICENSE file.

// Package scheduler provides a simple, humans-friendly way to schedule the execution of the go function.
// It includes delay execution and periodic execution.
package scheduler

import (
	"crypto/md5"
	"errors"
	"fmt"
	"io"
	"reflect"
	"sync"
	"time"
)

type Every int
type Sched int

const (
	EveryMinute Every = iota + 1
	EveryHour
	EveryDay
	EverySecond
)

const (
	Second Sched = iota + 1
	Minute
	Hour
	Day
)

// EveryRune is value of job sched, like "*" in cron, represents every
// second/minute/hour/day/weekday/month.
const EveryRune = 0
const EveryDayWeekMonth = 1

var (
	// defaultJobSetSize default size for job set
	defaultJobSetSize = 5000

	// maxJobSetSize maximum size for job set
	maxJobSetSize = 10000

	// ErrPendingJob is returned when the pending job not exist
	ErrPendingJob = errors.New("pending job not exist")

	// ErrOverlength is returned when the job size over maxJobSetSize variable
	ErrOverlength = errors.New("job set size overlength")

	// ErrJobSched is returned when the job sched not exist,
	// under normal circumstances, this error will not occur,
	// unless the key definition of job sched is incorrectly modified.
	ErrJobSched = errors.New("job sched not exist")

	// ErrTimeNegative is returned when the time argument is negative
	ErrTimeNegative = errors.New("time argument can not be negative")

	// ErrDupJobID is returned when the generateID generates the same id
	ErrDupJobID = errors.New("duplicate job id")

	// ErrAlreadyComplayed is returned when cancel a completed job
	ErrAlreadyComplayed = errors.New("Job hash been completed")

	// ErrCancelJob is returned when time.Timer.Stop function occur error
	ErrCancelJob = errors.New("cancel job failed")

	// ErrRangeSecond is returned when Second method argument is not int
	ErrRangeSecond = errors.New("argument 0 <= n <= 59 in Second method")

	// ErrRangeMinute is returned when Minute method argument is not int
	ErrRangeMinute = errors.New("argument 0 <= n <= 59 in Minute method")

	// ErrRangeHour is returned when Hour method argument is not int
	ErrRangeHour = errors.New("argument 0 <= n <= 23 in Hour method")

	// ErrRangeDay is returned when Day method argument is not int
	ErrRangeDay = errors.New("argument 1 <= n <= 31 in Day method")

	// EmptySched represents an empty job sched
	EmptySched = map[Sched]int{}
)

// JobSet

// JobSet stores pending jobs and completed jobs and it is concurrent safly.
type JobSet struct {
	lock         sync.RWMutex    // ensure concurrent safe
	scheduledSet map[string]*Job // storage pending jobs
}

// Job

// JobTicker is the wrapper for time.Ticker, one job corresponds to a JobTicker.
type JobTicker struct {
	ID     string       // unique id
	ticker *time.Ticker // wrapper time.Ticker
}

// Job is an abstraction of a scheduling task.
type Job struct {
	ID   string // unique id
	Type Every

	// Sched is a job sched, like cron style but the order of time is not
	// fixed, can be arranged and combined at will.
	Sched map[Sched]int

	fn      interface{}   // job function
	args    []interface{} // function args
	JTicker *JobTicker    // JobTicker
}

// Second method set Second key for job sched.
func (j *Job) Second(n int) *Job {
	if n < 0 || n > 59 {
		panic(ErrRangeSecond)
	}

	j.Sched[Second] = n
	return j
}

// Minute method set Minute key for job sched.
func (j *Job) Minute(n int) *Job {
	if n < 0 || n > 59 {
		panic(ErrRangeMinute)
	}

	j.Sched[Minute] = n
	return j
}

// Hour method set Hour key for job sched.
func (j *Job) Hour(n int) *Job {
	if n < 0 || n > 23 {
		panic(ErrRangeHour)
	}

	j.Sched[Hour] = n
	return j
}

// Day method set Day key for job sched.
func (j *Job) Day(n int) *Job {
	if n < 1 || n > 31 {
		panic(ErrRangeDay)
	}

	j.Sched[Day] = n
	return j
}

// Do according to the job type and job sched execute job.
func (j *Job) Do(fn interface{}, args ...interface{}) (jobID string) {
	j.fn = fn
	j.args = args

	// initial job.JTicker (note: also can not put it in a new goroutine)
	j.JTicker = new(JobTicker)
	j.JTicker.ID = generateID()
	j.JTicker.ticker = time.NewTicker(1 * time.Second)
	var evaluation func(now time.Time) bool
	switch j.Type {
	case EverySecond:
		evaluation = func(now time.Time) bool {
			return now.Second()%j.Sched[Second] == 0
		}
	case EveryMinute:
		evaluation = func(now time.Time) bool {
			return (j.Sched[Second] == now.Second()) &&
				(now.Minute()%j.Sched[Minute] == 0)
		}
	case EveryHour:
		evaluation = func(now time.Time) bool {
			return (j.Sched[Second] == now.Second()) &&
				(j.Sched[Minute] == now.Minute()) &&
				(now.Hour()%j.Sched[Hour] == 0)
		}
	case EveryDay:
		evaluation = func(now time.Time) bool {
			return (j.Sched[Second] == now.Second()) &&
				(j.Sched[Minute] == now.Minute()) &&
				(j.Sched[Hour] == now.Hour()) &&
				(now.Day()%j.Sched[Day] == 0)
		}
	default:
		evaluation = func(now time.Time) bool {
			return false
		}
	}
	go func() {
		// begin ticktock...
		for t := range j.JTicker.ticker.C {
			_ = t
			now := time.Now()
			if evaluation(now) {
				j.run()
			}
		}
	}()

	return j.ID
}

// run funtion by reflect.
func (j *Job) run() {
	rFn := reflect.ValueOf(j.fn)
	rArgs := make([]reflect.Value, len(j.args))
	for i, v := range j.args {
		rArgs[i] = reflect.ValueOf(v)
	}

	// retry
	defer func() {
		if err := recover(); err != nil {
			time.Sleep(5 * time.Second) // wait for five seconds
			rFn.Call(rArgs)
		}
	}()

	rFn.Call(rArgs)
}

// Scheduler

// Scheduler is responsible for scheduling jobs.
type Scheduler struct {
	jobSetSize int    // custom size for job set, can not overlength maxJobSetSize
	js         JobSet // JobSet
}

// NewScheduler new Scheduler instance.
func NewScheduler(jss int) (*Scheduler, error) {
	if jss > maxJobSetSize {
		return nil, ErrOverlength
	}
	if jss <= 0 {
		jss = defaultJobSetSize
	}

	s := &Scheduler{
		jobSetSize: jss,
		js: JobSet{lock: sync.RWMutex{},
			scheduledSet: make(map[string]*Job, jss)},
	}
	return s, nil
}

// Every method schedule job with Every mode.
func (s *Scheduler) Every(interval Every) *Job {
	s.js.lock.Lock()
	defer s.js.lock.Unlock()

	// temporarily handle like this
	if len(s.js.scheduledSet) >= 10000 {
		panic("pending set is full")
	}

	// create job
	id := generateID()
	j := &Job{
		ID:   id,
		Type: interval,
		// Sched[...] = -1 <=> cron *
		Sched: InitJobSched(),
	}

	// put in pending job set
	s.js.scheduledSet[id] = j
	return j
}

// PendingJob get pending job by id.
func (s *Scheduler) GetJob(id string) (*Job, error) {
	s.js.lock.RLock()
	defer s.js.lock.RUnlock()

	if job, ok := s.js.scheduledSet[id]; ok {
		return job, nil
	}
	return nil, ErrPendingJob
}

// JobSched get job sched.
func (s *Scheduler) JobSched(id string) (map[Sched]int, error) {
	job, err := s.GetJob(id)
	if err != nil {
		return EmptySched, err
	}

	return job.Sched, nil
}

// CancelJob can cancel the job before scheduling.
func (s *Scheduler) CancelJob(id string) error {
	s.js.lock.RLock()
	defer s.js.lock.RUnlock()

	job, exists := s.js.scheduledSet[id]
	if !exists {
		return ErrPendingJob
	}
	job.JTicker.ticker.Stop()
	return nil
}

// generateID generate job id
func generateID() string {
	h := md5.New()
	_, err := io.WriteString(h, time.Now().String())
	if err != nil {
		panic(err)
	}
	id := fmt.Sprintf("%x", h.Sum(nil))
	return id
}

// InitJobSched initiate job sched by job type
func InitJobSched() map[Sched]int {
	return map[Sched]int{
		Second: EveryRune,
		Minute: EveryRune,
		Hour:   EveryRune,
		Day:    EveryDayWeekMonth,
	}
}
