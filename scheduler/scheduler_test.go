// Copyright (c) 2019, prprprus All rights reserved.
// Use of this source code is governed by a BSD-style .
// license that can be found in the LICENSE file.

package scheduler

import (
	"testing"
)

// Scheduler

func TestNewScheduler(t *testing.T) {
	_, err := NewScheduler(-1)
	if err != nil {
		t.Errorf("maxJobSetSize can be negative")
	}
	_, err = NewScheduler(10001)
	if err == nil {
		t.Errorf("maxJobSetSize overlength")
	}
}

// Job

func TestSecond(t *testing.T) {
	defer func() {
		if err := recover(); err != nil && err == ErrRangeSecond {
			return
		}
	}()
	s, _ := NewScheduler(10)
	j := s.Every(EveryMinute).Second(233)
	if j.Sched[Second] != 233 {
		t.Errorf("set second error")
	}
	// panic
	j.Second(-1)
}

func TestMinute(t *testing.T) {
	s, _ := NewScheduler(10)
	j := s.Every(EveryMinute).Minute(59)
	if j.Sched[Minute] != 59 {
		t.Errorf("set minute error")
	}
}

func TestHour(t *testing.T) {
	s, _ := NewScheduler(10)
	j := s.Every(EveryHour).Hour(12)
	if j.Sched[Hour] != 12 {
		t.Errorf("set hour error")
	}
}

func TestDay(t *testing.T) {
	s, _ := NewScheduler(10)
	j := s.Every(EveryDay).Day(24)
	if j.Sched[Day] != 24 {
		t.Errorf("set day error")
	}
}

// util

func TestInitJobSched(t *testing.T) {
	// Every
	s, _ := NewScheduler(10)
	j := s.Every(EveryMinute)
	for k, v := range j.Sched {
		if k != Second && k != Minute && k != Hour && k != Day {
			t.Errorf("Initial Every job sched failed")
		}
		if (v != 0) && (v != 1) {
			t.Errorf("Initial Every job sched failed")
		}
	}
}
