package db

type SortDirection int

const (
	// Ascending sort from bottom to up
	Ascending SortDirection = iota + 1
	// Descending sort from up to bottom
	Descending
)

var sortDirections = []string{
	"ASC",
	"DESC",
}

func (s SortDirection) String() string {
	return sortDirections[s-1]
}

// If we want to have multiple sort parameters make this a map
type SortParameter struct {
	Column    string
	Direction SortDirection
}
