DROP TABLE IF EXISTS neuta.exchanges CASCADE;
DROP TABLE IF EXISTS neuta.assettypes CASCADE;
DROP TABLE IF EXISTS neuta.assets CASCADE;
DROP SCHEMA IF EXISTS neuta CASCADE;
