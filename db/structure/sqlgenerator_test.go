package structure_test

import (
	"io/ioutil"
	"testing"

	"gitlab.com/porqueoutai/go-pkgs/db/structure"

	"github.com/stretchr/testify/assert"
)

type dbTestItemSuccess struct {
	wantedStateFile      string
	expectedSqlFile      string
	lastAppliedStateFile string
}

func TestDatabaseModelChange(t *testing.T) {
	assert := assert.New(t)
	var dataItem []dbTestItemSuccess = []dbTestItemSuccess{{wantedStateFile: "./testdata/successModel.yaml",
		expectedSqlFile: "./testdata/dropAndCreateOne.sql", lastAppliedStateFile: "./testdata/lastAppliedOne.yaml"},
		{wantedStateFile: "./testdata/columnNotEqual.yaml", expectedSqlFile: "./testdata/columnNotEqual.sql",
			lastAppliedStateFile: "./testdata/successModel.yaml"},
		{wantedStateFile: "./testdata/fkNotEqual.yaml", expectedSqlFile: "./testdata/fkNotEqual.sql",
			lastAppliedStateFile: "./testdata/successModel.yaml"},
		{wantedStateFile: "./testdata/schemaNotEqual.yaml", expectedSqlFile: "./testdata/schemaNotEqual.sql",
			lastAppliedStateFile: "./testdata/successModel.yaml"},
		{wantedStateFile: "./testdata/convenience.yaml", expectedSqlFile: "./testdata/convenience.sql",
			lastAppliedStateFile: "./testdata/lastAppliedOne.yaml"},
		{wantedStateFile: "./testdata/addDelete.yaml", expectedSqlFile: "./testdata/addDelete.sql",
			lastAppliedStateFile: "./testdata/lastAppliedOne.yaml"}}
	for _, item := range dataItem {
		wantedModel, err := readTestModelFile(item.wantedStateFile)
		assert.NoError(err)
		lastAppliedModel, err := readTestModelFile(item.lastAppliedStateFile)
		assert.NoError(err)
		expectedSql, err := readExpectedSqlFile(item.expectedSqlFile)
		assert.NoError(err)
		sql, desiredState, err := structure.DesiredDBState(wantedModel, lastAppliedModel)
		assert.NoError(err)
		assert.False(desiredState)
		assert.Equal(expectedSql, sql)
	}
}

func TestInitializeModel(t *testing.T) {
	assert := assert.New(t)
	wantedModel, err := readTestModelFile("./testdata/successModel.yaml")
	assert.NoError(err)
	expectedSql, err := readExpectedSqlFile("./testdata/successModel.sql")
	assert.NoError(err)
	sql, desiredState, err := structure.DesiredDBState(wantedModel, nil)
	assert.NoError(err)
	assert.False(desiredState)
	assert.Equal(expectedSql, sql)
}

func TestModelsIdentical(t *testing.T) {
	assert := assert.New(t)
	wantedModel, err := readTestModelFile("./testdata/successModel.yaml")
	assert.NoError(err)
	sql, desiredState, err := structure.DesiredDBState(wantedModel, wantedModel)
	assert.NoError(err)
	assert.True(desiredState)
	assert.Equal("", sql)
}

func TestLastAppliedFailure(t *testing.T) {
	assert := assert.New(t)
	wantedModel, err := readTestModelFile("./testdata/wantedFailure.yaml")
	assert.NoError(err)
	expectedSql, err := readExpectedSqlFile("./testdata/wantedFailure.sql")
	assert.NoError(err)
	sql, desiredState, err := structure.DesiredDBState(wantedModel, []byte("yaml no"))
	assert.NoError(err)
	assert.False(desiredState)
	assert.Equal(expectedSql, sql)
}

func TestInvalidYamlModel(t *testing.T) {
	sql, desiredState, err := structure.DesiredDBState([]byte("fail"), nil)
	assert.Error(t, err)
	assert.False(t, desiredState)
	assert.Equal(t, "", sql)
}

func TestNoActualStateSpecified(t *testing.T) {
	assert := assert.New(t)
	lastAppliedModel, err := readTestModelFile("./testdata/successModel.yaml")
	assert.NoError(err)
	expectedSql, err := readExpectedSqlFile("./testdata/emptyWanted.sql")
	assert.NoError(err)
	sql, desiredState, err := structure.DesiredDBState([]byte("yaml: yes"), lastAppliedModel)
	assert.NoError(err)
	assert.False(desiredState)
	assert.Equal(expectedSql, sql)
}

func readTestModelFile(filename string) ([]byte, error) {
	bytes, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	return bytes, nil
}

func readExpectedSqlFile(filename string) (string, error) {
	bytes, err := ioutil.ReadFile(filename)
	if err != nil {
		return "", err
	}
	return string(bytes), nil
}
