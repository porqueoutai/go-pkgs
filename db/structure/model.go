package structure

import (
	"fmt"
	"strings"
)

type dbStructure struct {
	Tables  []table  `yaml:"tables"`
	Schemas []schema `yaml:"schemas"`
}

type schema struct {
	Name string `yaml:"name"`
}

type table struct {
	Name             string            `yaml:"name"`
	Schema           string            `yaml:"schema"`
	Columns          []column          `yaml:"columns"`
	ForeignKeys      []foreignKey      `yaml:"foreigncolumns"`
	TableConstraints []tableConstraint `yaml:"tableconstraints"`
}

type column struct {
	Name        string   `yaml:"name"`
	Datatype    string   `yaml:"datatype"`
	Constraints []string `yaml:"constraints"`
}

type foreignKey struct {
	Column       string `yaml:"name"`
	Schema       string `yaml:"schema"`
	ForeignTable string `yaml:"table"`
	Key          string `yaml:"foreignkey"`
}

type tableConstraint struct {
	Name       string   `yaml:"name"`
	Constraint string   `yaml:"constraint"`
	Columns    []string `yaml:"columns"`
}

func (d *dbStructure) equal(structure dbStructure) (bool, *strings.Builder) {
	if len(d.Schemas) != len(structure.Schemas) {
		return false, nil
	}
	equal, sql := tablesEqual(d.Tables, structure.Tables)
	if !equal && sql == nil {
		return false, nil
	}
	for _, schema := range structure.Schemas {
		if !containsSchema(schema, d.Schemas) {
			return false, nil
		}
	}
	return equal, sql
}

func (s *schema) equal(dbSchema schema) bool {
	return *s == dbSchema
}

func tablesEqual(old []table, new []table) (bool, *strings.Builder) {
	var sb strings.Builder
adding:
	for _, newTable := range new {
		for _, oldTable := range old {
			if newTable.Name == oldTable.Name && newTable.Schema == oldTable.Schema {
				equal, sql := oldTable.equal(newTable)
				if !equal && sql == nil {
					return false, nil
				}
				if !equal && sql != nil {
					sb.WriteString(sql.String())
				}
				continue adding
			}
		}
		addTable(&sb, newTable)
	}
dropping:
	for _, oldTable := range old {
		for _, newTable := range new {
			if newTable.Name == oldTable.Name && newTable.Schema == oldTable.Schema {
				continue dropping
			}
		}
		dropTable(&sb, oldTable)
	}
	return sb.Len() == 0, &sb
}

func (t *table) equal(tb table) (bool, *strings.Builder) {
	if t.Name != tb.Name || t.Schema != tb.Schema || len(t.ForeignKeys) != len(tb.ForeignKeys) || len(t.TableConstraints) != len(tb.TableConstraints) {
		return false, nil
	}
	equal, sql := columnsEqual(t.Columns, tb.Columns, fmt.Sprintf("%s.%s", tb.Schema, tb.Name))
	if !equal && sql == nil {
		return false, nil
	}

	for _, fk := range tb.ForeignKeys {
		if !containsFK(fk, t.ForeignKeys) {
			return false, nil
		}
	}
	for _, tc := range tb.TableConstraints {
		if !containsTC(tc, t.TableConstraints) {
			return false, nil
		}
	}
	return equal, sql
}

func columnsEqual(old []column, new []column, tableName string) (bool, *strings.Builder) {
	var sb strings.Builder
adding:
	for _, newCol := range new {
		for _, oldCol := range old {
			if newCol.Name == oldCol.Name {
				equal := oldCol.equal(newCol)
				if !equal {
					return false, nil
				}
				continue adding
			}
		}
		alterAddColumn(&sb, newCol, tableName)
	}
dropping:
	for _, oldCol := range old {
		for _, newCol := range new {
			if oldCol.Name == newCol.Name {
				continue dropping
			}
		}
		alterDropColumn(&sb, oldCol, tableName)
	}
	return sb.Len() == 0, &sb
}

func (t *tableConstraint) equal(tc tableConstraint) bool {
	if t.Constraint != tc.Constraint || len(t.Columns) != len(tc.Columns) || t.Name != tc.Name {
		return false
	}
	for _, column := range tc.Columns {
		if !contains(column, t.Columns) {
			return false
		}
	}
	return true
}

func (f *foreignKey) equal(fk foreignKey) bool {
	return *f == fk
}

// TBA: If columns are not equal will return false and a string to transform the column in the desired state
// TBA: If columns are equal will return true and an empty string
func (c *column) equal(col column) bool {
	if c.Datatype != col.Datatype || len(c.Constraints) != len(col.Constraints) || c.Name != col.Name {
		return false
	}
	for _, constraint := range col.Constraints {
		if !contains(constraint, c.Constraints) {
			return false
		}
	}
	return true
}

func contains(str string, list []string) bool {
	for _, element := range list {
		if element == str {
			return true
		}
	}
	return false
}

func containsFK(fk foreignKey, fks []foreignKey) bool {
	for _, key := range fks {
		if key.equal(fk) {
			return true
		}
	}
	return false
}

func containsTC(tc tableConstraint, tcs []tableConstraint) bool {
	for _, constraint := range tcs {
		if constraint.equal(tc) {
			return true
		}
	}
	return false
}

func containsSchema(s schema, schemas []schema) bool {
	for _, dbSchema := range schemas {
		if dbSchema.equal(s) {
			return true
		}
	}
	return false
}
