package structure

import (
	"strings"

	"gopkg.in/yaml.v3"
)

const linebreak string = "\n"

func DesiredDBState(actualModel []byte, lastAppliedModel []byte) (string, bool, error) {
	var wantedDBModel dbStructure
	err := yaml.Unmarshal(actualModel, &wantedDBModel)
	if err != nil {
		return "", false, err
	}
	var lastDBModel dbStructure
	var sb strings.Builder
	if len(lastAppliedModel) == 0 {
		createDatabaseStructure(&sb, wantedDBModel)
		return sb.String(), false, nil
	}
	err = yaml.Unmarshal(lastAppliedModel, &lastDBModel)
	if err != nil {
		// We do not know in what kind of state our DB is, so we drop/create atleast our wanted structure to be sure application is running
		dropAndCreateStructure(&sb, wantedDBModel, wantedDBModel)
		return sb.String(), false, nil
	}
	equal, sql := lastDBModel.equal(wantedDBModel)
	if !equal && sql != nil {
		return sql.String(), false, nil
	}
	if !equal && sql == nil {
		dropAndCreateStructure(&sb, wantedDBModel, lastDBModel)
		return sb.String(), false, nil
	}
	return "", true, nil
}

func dropAndCreateStructure(sb *strings.Builder, createDBStruct dbStructure, dropDBStruct dbStructure) {
	dropDatabaseStructure(sb, dropDBStruct)
	createDatabaseStructure(sb, createDBStruct)
}
func dropDatabaseStructure(sb *strings.Builder, databaseStructure dbStructure) {
	for _, table := range databaseStructure.Tables {
		dropTable(sb, table)
	}
	for _, schema := range databaseStructure.Schemas {
		dropSchema(sb, schema)
	}
}

func createDatabaseStructure(sb *strings.Builder, databaseStructure dbStructure) {
	for _, schema := range databaseStructure.Schemas {
		addSchema(sb, schema)
	}
	for _, table := range databaseStructure.Tables {
		addTable(sb, table)
	}
}
func addSchema(sb *strings.Builder, dbSchema schema) {
	sb.WriteString("CREATE SCHEMA " + "IF NOT EXISTS " + dbSchema.Name + ";" + linebreak)
}

// We could check if table name exists
func addTable(sb *strings.Builder, dbTable table) {
	sb.WriteString("CREATE TABLE IF NOT EXISTS ")
	if dbTable.Schema != "" {
		sb.WriteString(dbTable.Schema + ".")
	}
	sb.WriteString(dbTable.Name + " (\n")
	addColumns(sb, dbTable.Columns, len(dbTable.ForeignKeys) == 0 && len(dbTable.TableConstraints) == 0)
	addForeingKeys(sb, dbTable.ForeignKeys, len(dbTable.TableConstraints) == 0)
	addTableConstraints(sb, dbTable.TableConstraints)
	sb.WriteString(");" + linebreak)
}

func addColumns(sb *strings.Builder, columns []column, omitLastComma bool) {
	for idx, tbColumn := range columns {
		addColumn(sb, tbColumn)
		if idx != (len(columns)-1) || !omitLastComma {
			sb.WriteString(",")
		}
		sb.WriteString(linebreak)
	}
}

// We could check for type and name not empty
func addColumn(sb *strings.Builder, tableColumn column) {
	sb.WriteString(tableColumn.Name + " " + tableColumn.Datatype)
	for _, constraint := range tableColumn.Constraints {
		sb.WriteString(" " + constraint)
	}
}

func addForeingKeys(sb *strings.Builder, foreignKeys []foreignKey, omitLastComma bool) {
	for idx, fk := range foreignKeys {
		addForeingKey(sb, fk)
		if idx != (len(foreignKeys)-1) || !omitLastComma {
			sb.WriteString(",")
		}
		sb.WriteString(linebreak)
	}
}
func addForeingKey(sb *strings.Builder, fk foreignKey) {
	sb.WriteString("FOREIGN KEY (" + fk.Column + ") REFERENCES ")
	if fk.Schema != "" {
		sb.WriteString(fk.Schema + ".")
	}
	sb.WriteString(fk.ForeignTable + " (" + fk.Key + ")")
}

func addTableConstraints(sb *strings.Builder, tbConstraints []tableConstraint) {
	for idx, tbConstraint := range tbConstraints {
		addTableConstraint(sb, tbConstraint)
		if idx != (len(tbConstraints) - 1) {
			sb.WriteString(",")
		}
		sb.WriteString(linebreak)
	}
}

func addTableConstraint(sb *strings.Builder, tbConstraint tableConstraint) {
	sb.WriteString("CONSTRAINT " + tbConstraint.Name + " " + tbConstraint.Constraint + " (")
	for idx, column := range tbConstraint.Columns {
		sb.WriteString(column)
		if idx != (len(tbConstraint.Columns) - 1) {
			sb.WriteString(",")
		}
	}
	sb.WriteString(")")
}

func dropTable(sb *strings.Builder, tb table) {
	sb.WriteString("DROP TABLE IF EXISTS ")
	if tb.Schema != "" {
		sb.WriteString(tb.Schema + ".")
	}
	sb.WriteString(tb.Name + " CASCADE;" + linebreak)
}

func dropSchema(sb *strings.Builder, dbSchema schema) {
	sb.WriteString("DROP SCHEMA IF EXISTS " + dbSchema.Name + " CASCADE;" + linebreak)
}

func alterAddColumn(sb *strings.Builder, tableColumn column, tableName string) {
	sb.WriteString("ALTER TABLE " + tableName + " ADD ")
	addColumn(sb, tableColumn)
	sb.WriteString(";" + linebreak)
}

func alterDropColumn(sb *strings.Builder, tableColumn column, tableName string) {
	sb.WriteString("ALTER TABLE " + tableName + " DROP COLUMN " + tableColumn.Name + ";" + linebreak)
}
