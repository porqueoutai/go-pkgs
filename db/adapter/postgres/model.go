package postgres

type ForeignColumn struct {
	Column string
	Table  string
	Schema string
}

func (f ForeignColumn) getTable() string {
	return f.Schema + "." + f.Table
}
