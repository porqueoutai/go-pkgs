package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"reflect"

	"github.com/rs/zerolog/log"
	"gitlab.com/porqueoutai/go-pkgs/db"

	"github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"
	"github.com/jmoiron/sqlx/reflectx"
)

type Holder struct {
	session *sqlx.DB
	table   string
	dbname  string
	mapper  *reflectx.Mapper

	columns        []string
	foreignColumns map[string]ForeignColumn
}

func NewCRUDTable(session *sqlx.DB, db, name, schema string, columns []string, foreign map[string]ForeignColumn) *Holder {
	return &Holder{
		session:        session,
		table:          schema + "." + name,
		dbname:         db,
		columns:        columns,
		mapper:         reflectx.NewMapper("db"),
		foreignColumns: foreign,
	}
}

func (h *Holder) CreateEntries(ctx context.Context, objects []interface{}) error {
	if len(objects) == 0 {
		return nil
	}
	columns := h.determineColumns(objects[0])
	var sqlQuery squirrel.InsertBuilder = squirrel.Insert(h.table).Columns(columns...)

	val := reflect.ValueOf(objects)
	kind := val.Kind()
	if kind != reflect.Slice && kind != reflect.Array {
		panic(&reflect.ValueError{Method: "CreateEntries", Kind: kind})
	}

	l := val.Len()
	columnCount := len(columns)
	for i := 0; i < l; i++ {
		data := val.Index(i).Interface()
		mapping := h.mapper.FieldMap(reflect.ValueOf(data))
		values := make([]interface{}, columnCount)
		for i, value := range columns {
			values[i] = mapping[value].Interface()
		}
		sqlQuery = sqlQuery.Values(values...)
	}
	sqlQuery = sqlQuery.PlaceholderFormat(squirrel.Dollar)
	query, args, err := sqlQuery.ToSql()
	if err != nil {
		return fmt.Errorf("Postgresql: unable to build query: %w", err)
	}
	log.Trace().Str("table", h.table).Str("query", query).Interface("args", args).Msg("Query for creating an entry")
	stmt, err := h.session.PreparexContext(ctx, query)
	if err != nil {
		return fmt.Errorf("Postgresql: unable to prepare query: %w", err)
	}
	defer stmt.Close()
	_, err = stmt.ExecContext(ctx, args...)
	if err != nil {
		return fmt.Errorf("Postgresql: unable to execute query: %w", err)
	}
	return nil
}

func (h *Holder) CreateEntry(ctx context.Context, data interface{}) error {
	objects := make([]interface{}, 1)
	objects[0] = data
	return h.CreateEntries(ctx, objects)
}

func (h *Holder) DeleteEntry(ctx context.Context, filter interface{}) (int64, error) {
	var sqlQuery squirrel.DeleteBuilder = squirrel.Delete(h.table).Where(filter).
		PlaceholderFormat(squirrel.Dollar)
	query, args, err := sqlQuery.ToSql()
	log.Trace().Str("table", h.table).Str("query", query).Interface("args", args).Msg("Query for deleting entry in table")
	if err != nil {
		return 0, fmt.Errorf("Postgresql: unable to build query: %w", err)
	}
	stmt, err := h.session.PreparexContext(ctx, query)
	if err != nil {
		return 0, fmt.Errorf("Postgresql: unable to prepare query: %w", err)
	}
	defer stmt.Close()
	result, err := stmt.ExecContext(ctx, args...)
	if err != nil {
		return 0, fmt.Errorf("Postgresql: unable to execute query: %w", err)
	}
	count, err := result.RowsAffected()
	if err != nil {
		return 0, fmt.Errorf("Postgresql: unable to get results of query: %w", err)
	}
	return count, err
}

func (h *Holder) WhereCount(ctx context.Context, filter interface{}) (int64, error) {
	var sqlQuery squirrel.SelectBuilder = squirrel.Select("COUNT(*) as count").From(h.table).PlaceholderFormat(squirrel.Dollar)
	if filter != nil {
		sqlQuery = sqlQuery.Where(filter)
	}
	query, args, err := sqlQuery.ToSql()
	log.Trace().Str("table", h.table).Str("query", query).Interface("args", args).Msg("Query for getting count of table")
	if err != nil {
		return 0, fmt.Errorf("Postgresql: unable to build query: %w", err)
	}
	stmt, err := h.session.PreparexContext(ctx, query)
	if err != nil {
		return 0, fmt.Errorf("Postgresql: unable to prepare query: %w", err)
	}
	defer stmt.Close()
	var count int64
	err = stmt.QueryRowContext(ctx, args...).Scan(&count)
	if err == sql.ErrNoRows {
		return 0, nil
	} else if err != nil {
		return 0, fmt.Errorf("Postgresql: unable to execute query: %w", err)
	}
	return count, nil
}

// If no rows were found returns a sql.ErrNoRows error which must be handled by the client
func (h *Holder) GetOneWhere(ctx context.Context, filters []interface{}, result interface{}, sort db.SortParameter) error {
	var sqlQuery squirrel.SelectBuilder = squirrel.Select(h.columns...).From(h.table).
		Limit(1).PlaceholderFormat(squirrel.Dollar)
	for _, filter := range filters {
		sqlQuery = sqlQuery.Where(filter)
	}
	sqlQuery = h.joinForeignTables(sqlQuery)
	if sort.Column != "" {
		sqlQuery = sqlQuery.OrderBy(fmt.Sprintf("%s %s", sort.Column, sort.Direction.String()))
	}
	query, args, err := sqlQuery.ToSql()
	log.Trace().Str("table", h.table).Str("query", query).Interface("args", args).Msg("Query for getting one element of table")
	if err != nil {
		return fmt.Errorf("Postgresql: unable to build query: %w", err)
	}
	stmt, err := h.session.PreparexContext(ctx, query)
	if err != nil {
		return fmt.Errorf("Postgresql: unable to prepare query: %w", err)
	}
	defer stmt.Close()
	err = h.session.QueryRowxContext(ctx, query, args...).StructScan(result)
	if err == sql.ErrNoRows {
		return err
	} else if err != nil {
		return fmt.Errorf("Postgresql: unable to execute query: %w", err)
	}
	return nil
}

func (h *Holder) ForeignKeyIDForValue(ctx context.Context, filter interface{}, result interface{}, column string) error {
	value, exist := h.foreignColumns[column]
	if !exist {
		return fmt.Errorf("Postgresql: column %s does not exist in CRUD table", column)
	}
	var sqlQuery squirrel.SelectBuilder = squirrel.Select(value.Column).From(value.getTable()).Where(filter).
		Limit(1).PlaceholderFormat(squirrel.Dollar)
	query, args, err := sqlQuery.ToSql()
	log.Trace().Str("table", h.table).Str("query", query).Interface("args", args).Msg("Query for retrieving fk id")
	if err != nil {
		return fmt.Errorf("Postgresql: unable to build query: %w", err)
	}
	stmt, err := h.session.PrepareContext(ctx, query)
	if err != nil {
		return fmt.Errorf("Postgresql: unable to prepare query: %w", err)
	}
	defer stmt.Close()
	err = h.session.QueryRowxContext(ctx, query, args...).Scan(result)
	if err == sql.ErrNoRows {
		return err
	} else if err != nil {
		return fmt.Errorf("Postgresql: unable to execute query: %w", err)
	}
	return nil
}

// ToDo for better concurrency mechanism: implement pagination
// ToDo introduce sort parameters with asc/desc
func (h *Holder) Search(ctx context.Context, filters []interface{}, result interface{}, sort db.SortParameter) error {
	var sqlQuery squirrel.SelectBuilder = squirrel.Select(h.columns...).From(h.table).PlaceholderFormat(squirrel.Dollar)
	for _, filter := range filters {
		sqlQuery = sqlQuery.Where(filter)
	}
	sqlQuery = h.joinForeignTables(sqlQuery)
	if sort.Column != "" {
		sqlQuery = sqlQuery.OrderBy(fmt.Sprintf("%s %s", sort.Column, sort.Direction.String()))
	}
	query, args, err := sqlQuery.ToSql()
	log.Trace().Str("table", h.table).Str("query", query).Interface("args", args).Msg("Query for searching in table")
	if err != nil {
		return fmt.Errorf("Postgresql: unable to build query: %w", err)
	}
	stmt, err := h.session.PreparexContext(ctx, query)
	if err != nil {
		return fmt.Errorf("Postgresql: unable to prepare query: %w", err)
	}
	defer stmt.Close()
	err = stmt.SelectContext(ctx, result, args...)
	if err == sql.ErrNoRows {
		return err
	} else if err != nil {
		return fmt.Errorf("Postgresql: unable to execute query: %w", err)
	}
	return nil
}

func (h *Holder) determineColumns(data interface{}) []string {
	mapping := h.mapper.FieldMap(reflect.ValueOf(data))
	var columns []string
	for column := range mapping {
		columns = append(columns, column)
	}
	return columns
}

func (h *Holder) joinForeignTables(builder squirrel.SelectBuilder) squirrel.SelectBuilder {
	for key, ftable := range h.foreignColumns {
		builder = builder.LeftJoin(fmt.Sprintf("%s %s ON %s = %s.%s", ftable.getTable(), ftable.Table, key, ftable.Table, ftable.Column))
	}
	return builder
}
