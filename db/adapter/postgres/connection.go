package postgres

import (
	"fmt"
	"time"

	_ "github.com/jackc/pgx/v4/stdlib"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

type DBConfiguration struct {
	Host     string
	Port     string
	Database string
	User     string
	Password string
}

func InitializeConnection(config *DBConfiguration) (*sqlx.DB, error) {
	var defaultDriver string = "pgx"
	// If we need tracing we can easily implement https://opencensus.io
	connection, err := sqlx.Open(defaultDriver, config.String())
	if err != nil {
		return nil, fmt.Errorf("Postgresql: unable to open driver: %w", err)
	}
	err = connection.Ping()
	if err != nil {
		return nil, fmt.Errorf("Postgresql: unable to ping database: %w", err)
	}
	connection.SetMaxIdleConns(1)
	connection.SetMaxOpenConns(100)
	connection.SetConnMaxLifetime(6 * time.Minute)
	return connection, nil
}

func (d DBConfiguration) String() string {
	return fmt.Sprintf("host=%s port=%s user=%s password=%s database=%s sslmode=disable",
		d.Host, d.Port, d.User, d.Password, d.Database)
}
