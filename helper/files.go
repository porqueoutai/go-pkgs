package helper

import (
	"encoding/json"
	"io/ioutil"

	"gopkg.in/yaml.v3"
)

func JsonFileUnmarshall(filename string, result interface{}) error {
	bytes, err := ioutil.ReadFile(filename)
	if err != nil {
		return err
	}
	return json.Unmarshal(bytes, result)
}

func YamlFileUnmarshall(filename string, result interface{}) error {
	bytes, err := ioutil.ReadFile(filename)
	if err != nil {
		return err
	}
	return yaml.Unmarshal(bytes, result)
}
