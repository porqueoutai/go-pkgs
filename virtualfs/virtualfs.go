package virtualfs

import (
	"fmt"
	"os"
	"strings"

	"github.com/rs/zerolog/log"
)

type VirtualFS struct {
	paths []string
}

var virtualfs VirtualFS

func Init() {
	registerPath("./")
	registerPath("/etc/neuta/")
}

func registerPath(path string) {
	if !strings.HasSuffix(path, "/") {
		panic(fmt.Sprintf("Invalid path given '%s' - missing trailing slash", path))
	}
	_, err := os.Stat(path)
	if os.IsNotExist(err) {
		log.Debug().Msg(fmt.Sprintf("Failed to register path '%s'", path))
	} else {
		virtualfs.paths = append(virtualfs.paths, path)
		log.Debug().Msg(fmt.Sprintf("Register path '%s'", path))
	}
}

func GetFilePath(file string) string {
	for _, path := range virtualfs.paths {
		fullPath := path + file
		_, err := os.Stat(fullPath)
		if os.IsNotExist(err) {
			log.Debug().Msg(fmt.Sprintf("could not find '%s' at '%s'", file, fullPath))
			continue
		}
		log.Debug().Msg(fmt.Sprintf("path for '%s': '%s'", file, fullPath))
		return fullPath
	}
	return ""
}
