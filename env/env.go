package env

import "os"

func GetEnv(key, deflt string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return deflt
}

func GetEnvWithoutDefault(key string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return ""
}
