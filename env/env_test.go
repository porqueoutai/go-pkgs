package env_test

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/porqueoutai/go-pkgs/env"
)

func TestEnvDefault(t *testing.T) {
	value := env.GetEnv("FOOBAR_DOES_NOT_EXIST", "insanedefault")
	assert.Equal(t, value, "insanedefault")
}

func TestEnvNoDefault(t *testing.T) {
	os.Setenv("MYUSER", "lelele")
	value := env.GetEnv("MYUSER", "insanedefault")
	assert.NotEqual(t, value, "insanedefault")
}

func TestEnvWithoutDefault(t *testing.T) {
	os.Setenv("MYFOOBAR", "lelele")
	value := env.GetEnvWithoutDefault("MYFOOBAR")
	assert.Equal(t, value, "lelele")
}

func TestEnvEmpty(t *testing.T) {
	value := env.GetEnvWithoutDefault("NONEXISTING")
	assert.Equal(t, value, "")
}
