package throttling

import (
	"fmt"
	"sync"
	"sync/atomic"
	"time"

	"github.com/rs/zerolog/log"
)

type Token struct {
	id uint64
}

type RequestLimiter interface {
	// blocking call that gets you a token once one is available.
	Acquire() (*Token, error)
	// usually this is automatically done and only Acquire() should get called. But there
	// might be scenarios where you would like to manage this on your own
	Release(*Token)
	// delay delivering of tokens for the given amount of milliseconds
	StopFor(millis uint64)
}

type tokenManager struct {
	errorChannel          chan error
	releaseChan           chan *Token
	tokenChannel          chan *Token
	requestChannel        chan struct{}
	waitingForToken       int64
	activeTokens          map[uint64]*Token
	tokensMutex           sync.Mutex
	maxRequestsSuperior   int64
	intervalSuperior      int
	maxRequestsPerSeconds int64
	defaultRPS            int64
	issuedTokens          int64
	second                int
	nextTokenId           uint64
	waitTime              uint64
}

func (m *tokenManager) Acquire() (*Token, error) {
	go func() {
		m.requestChannel <- struct{}{}
	}()

	waitTime := atomic.LoadUint64(&m.waitTime)
	if atomic.CompareAndSwapUint64(&m.waitTime, waitTime, 0) {
		log.Trace().Msg(fmt.Sprintf("Waiting for %d millis", waitTime))
		time.Sleep(time.Duration(waitTime) * time.Millisecond)
	}
	select {
	case t := <-m.tokenChannel:
		return t, nil
	case err := <-m.errorChannel:
		return nil, err
	}
}

// you should not call this manually - except in case you got an
// error and don't wanna waste the free slot
func (m *tokenManager) Release(t *Token) {
	go func() {
		m.releaseChan <- t
	}()
}

func max(x, y uint64) uint64 {
	if x < y {
		return y
	}
	return x
}

func (m *tokenManager) StopFor(millis uint64) {
	old := atomic.LoadUint64(&m.waitTime)
	waitTime := max(old, millis)
	log.Debug().Msg(fmt.Sprintf("Stop token delivery for %d", waitTime))
	atomic.CompareAndSwapUint64(&m.waitTime, old, waitTime)
}

func newTokenManager(limitSeconds, limitSuperior int64, intervalSecSuperior int) *tokenManager {
	m := &tokenManager{
		errorChannel:          make(chan error),
		tokenChannel:          make(chan *Token),
		requestChannel:        make(chan struct{}),
		activeTokens:          make(map[uint64]*Token),
		releaseChan:           make(chan *Token),
		maxRequestsSuperior:   limitSuperior,
		intervalSuperior:      intervalSecSuperior,
		maxRequestsPerSeconds: limitSeconds,
		defaultRPS:            limitSeconds,
		issuedTokens:          0,
		waitingForToken:       0,
		nextTokenId:           0,
		waitTime:              0,
		second:                0,
	}

	go func() {
		ticker := time.NewTicker(1 * time.Second)
		for range ticker.C {
			if m.second == m.intervalSuperior {
				m.second = 0
				atomic.StoreInt64(&m.maxRequestsPerSeconds, m.defaultRPS)
				atomic.StoreInt64(&m.issuedTokens, 0)
			}
			diff := m.maxRequestsSuperior - atomic.LoadInt64(&m.issuedTokens)
			if diff > 0 {
				if diff >= m.defaultRPS {
					atomic.StoreInt64(&m.maxRequestsPerSeconds, m.defaultRPS)
				} else {
					atomic.StoreInt64(&m.maxRequestsPerSeconds, diff)
				}
				m.tokensMutex.Lock()
				for _, token := range m.activeTokens {
					go func(t *Token) {
						m.releaseChan <- t
					}(token)
				}
				m.tokensMutex.Unlock()
			}
			m.second += 1
		}
	}()

	return m
}

func (m *tokenManager) incIssuedToken() {
	atomic.AddInt64(&m.issuedTokens, 1)
}

func (m *tokenManager) incRequestedToken() {
	atomic.AddInt64(&m.waitingForToken, 1)
}

func (m *tokenManager) decRequestedToken() {
	atomic.AddInt64(&m.waitingForToken, -1)
}

func (m *tokenManager) hasRequestedTokens() bool {
	return atomic.LoadInt64(&m.waitingForToken) > 0
}

func (m *tokenManager) tryGenerateToken() {
	if m.isLimitExceeded() {
		m.incRequestedToken()
		return
	}
	atomic.AddUint64(&m.nextTokenId, 1)
	token := &Token{m.nextTokenId}

	m.tokensMutex.Lock()
	m.activeTokens[token.id] = token
	m.tokensMutex.Unlock()
	m.incIssuedToken()

	go func() {
		m.tokenChannel <- token
	}()
}

func (m *tokenManager) isLimitExceeded() bool {
	return len(m.activeTokens) >= int(atomic.LoadInt64(&m.maxRequestsPerSeconds))
}

func (m *tokenManager) releaseToken(token *Token) {
	m.tokensMutex.Lock()
	if _, ok := m.activeTokens[token.id]; !ok {
		m.tokensMutex.Unlock()
		return
	}

	delete(m.activeTokens, token.id)
	m.tokensMutex.Unlock()

	if m.hasRequestedTokens() {
		m.decRequestedToken()
		go m.tryGenerateToken()
	}
}

//If no limit for second, set same as minutes
func NewRequestLimiter(limitSeconds, limitSuperior, intervalSecSuperior int) (RequestLimiter, error) {
	if limitSeconds > limitSuperior {
		return nil, fmt.Errorf("Failed to create request limiter - seconds limit %d is greater than superior limit %d",
			limitSeconds, limitSuperior)
	}
	m := newTokenManager(int64(limitSeconds), int64(limitSuperior), intervalSecSuperior)

	go func() {
		for {
			select {
			case <-m.requestChannel:
				m.tryGenerateToken()
			case t := <-m.releaseChan:
				m.releaseToken(t)
			}
		}
	}()

	return m, nil
}
