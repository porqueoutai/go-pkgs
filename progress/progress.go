package progress

import (
	"sync/atomic"
)

type Progress struct {
	current   int32
	max       int32
	threshold int32
}

func (p *Progress) Count(steps int32) {
	atomic.AddInt32(&p.current, steps)
}

func (p *Progress) Progress() float32 {
	current := atomic.LoadInt32(&p.current)
	if current%p.threshold == 0 {
		percent := float32(current) * 100.0 / float32(p.max)
		return percent
	}
	return 0.0
}

func NewProgress(max int32) *Progress {
	return &Progress{
		current:   0,
		max:       max,
		threshold: 1000,
	}
}
