package logconfig

import (
	"github.com/rs/zerolog"
)

func InitLogging() {
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
}

func SetLogLevel(loglevel string) {
	if loglevel == "disabled" {
		zerolog.SetGlobalLevel(zerolog.Disabled)
	} else {
		level, err := zerolog.ParseLevel(loglevel)
		if err != nil {
			zerolog.SetGlobalLevel(zerolog.WarnLevel)
		} else {
			zerolog.SetGlobalLevel(level)
		}
	}
}
