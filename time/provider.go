package time

import "time"

type TimeProvider interface {
	Now() time.Time
	Sleep(d time.Duration)
	NewTicker(d time.Duration) *time.Ticker
}

type provider string

func NewTimeProvider() TimeProvider {
	var provider provider
	return &provider
}

func (p provider) Now() time.Time {
	return time.Now().UTC()
}

func (p provider) Sleep(d time.Duration) {
	time.Sleep(d)
}

func (p provider) NewTicker(d time.Duration) *time.Ticker {
	return time.NewTicker(d)
}
