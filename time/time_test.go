package time_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/porqueoutai/go-pkgs/time"
)

func TestMonthThirtyOneDays(t *testing.T) {
	t.Parallel()
	//1.06.2020 16:00:00
	end, start := time.GetFirstDayOfMonthAndPrior(1591027200)
	assert.Equal(t, int64(1590969600), end)
	assert.Equal(t, int64(1588291200), start)
}

func TestMonthThirtyDays(t *testing.T) {
	t.Parallel()
	//21.05.2020 14:00:00
	end, start := time.GetFirstDayOfMonthAndPrior(1590069600)
	assert.Equal(t, int64(1588291200), end)
	assert.Equal(t, int64(1585699200), start)
}

func TestMonthTwentyNineDays(t *testing.T) {
	t.Parallel()
	//03.03.2020 12:50:00
	end, start := time.GetFirstDayOfMonthAndPrior(1583239800)
	assert.Equal(t, int64(1583020800), end)
	assert.Equal(t, int64(1580515200), start)
}

func TestMonthTwentyEightDays(t *testing.T) {
	t.Parallel()
	//15.03.2021 00:00:00
	end, start := time.GetFirstDayOfMonthAndPrior(1615766400)
	assert.Equal(t, int64(1614556800), end)
	assert.Equal(t, int64(1612137600), start)
}
