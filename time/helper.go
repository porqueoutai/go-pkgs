package time

import "time"

var timeFormat = time.RFC3339

func FormatTime(time time.Time) string {
	return time.Format(timeFormat)
}

func ParseTime(value string) (time.Time, error) {
	return time.Parse(timeFormat, value)
}

func GetMonday(unixtimestamp int64) int64 {
	utcTime := time.Unix(unixtimestamp, 0).UTC()
	weekday := utcTime.Weekday()
	if weekday == 0 {
		weekday = 7
	}
	year, month, day := utcTime.Date()
	zeroDay := time.Date(year, month, day, 0, 0, 0, 0, time.UTC)
	diff := int(-1 * (weekday - 1))
	return zeroDay.Add(time.Duration(diff*24) * time.Hour).Unix()
}

func GetFirstDayOfMonthAndPrior(unixtimestamp int64) (int64, int64) {
	utcTime := time.Unix(unixtimestamp, 0).UTC()
	year, month, day := utcTime.Date()
	zeroDay := time.Date(year, month, day, 0, 0, 0, 0, time.UTC)
	var diff int
	if day > 1 {
		diff = int(-1 * (day - 1))
	}
	firstDay := zeroDay.Add(time.Duration(diff*24) * time.Hour)
	prior := firstDay.AddDate(0, -1, 0)
	return firstDay.Unix(), prior.Unix()
}
