package http_test

import (
	"errors"
	"net/http"
	"testing"

	"github.com/jarcoal/httpmock"
	"github.com/stretchr/testify/assert"
	_httpIntern "gitlab.com/porqueoutai/go-pkgs/http"
)

func TestHttpHappyPath(t *testing.T) {
	assert := assert.New(t)
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()
	httpmock.RegisterResponder("GET", "http://stockcandles.de/stock/candle",
		func(req *http.Request) (*http.Response, error) {
			return httpmock.NewStringResponse(200, "Test"), nil
		})
	var httpService _httpIntern.HttpService = _httpIntern.NewHttpService(8)
	ctx, err := _httpIntern.AssembleRequestCtx(http.MethodGet, "http://stockcandles.de/stock/candle",
		map[string][]string{"test": {"test"}}, nil, map[string]string{"Accept": "application/json"})
	assert.NoError(err)
	request, err := httpService.CreateRequest(ctx)
	assert.NoError(err)
	assert.NotNil(request)
	response, err := httpService.DoRequest(request)
	assert.NoError(err)
	assert.Equal(200, response.StatusCode)
	assert.NotNil(response)
}

func TestFailedToExecuteRequest(t *testing.T) {
	assert := assert.New(t)
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()
	httpmock.RegisterResponder("GET", "http://stockcandles.de",
		func(req *http.Request) (*http.Response, error) {
			return nil, errors.New("Failed to respond with valid answer")
		})
	var httpService _httpIntern.HttpService = _httpIntern.NewHttpService(8)
	ctx, err := _httpIntern.AssembleRequestCtx(http.MethodGet, "http://stockcandles.de", nil, nil, nil)
	assert.NoError(err)
	request, err := httpService.CreateRequest(ctx)
	assert.NoError(err)
	assert.NotNil(request)
	response, err := httpService.DoRequest(request)
	assert.Error(err)
	assert.Nil(response)
}

func TestCorruptUrl(t *testing.T) {
	var httpService _httpIntern.HttpService = _httpIntern.NewHttpService(8)
	ctx, err := _httpIntern.AssembleRequestCtx(http.MethodGet, "http://stockcandl{}s.de", nil, nil, nil)
	assert.Error(t, err)
	request, err := httpService.CreateRequest(ctx)
	assert.Nil(t, request)
	assert.Error(t, err)
}

func TestNonExistingHttpMethod(t *testing.T) {
	var httpService _httpIntern.HttpService = _httpIntern.NewHttpService(8)
	ctx, err := _httpIntern.AssembleRequestCtx("foo;", "http://stockcandles.de", nil, nil, nil)
	assert.NoError(t, err)
	request, err := httpService.CreateRequest(ctx)
	assert.Nil(t, request)
	assert.Error(t, err)
}

func TestMarshalBodyError(t *testing.T) {
	var httpService _httpIntern.HttpService = _httpIntern.NewHttpService(8)
	ctx, err := _httpIntern.AssembleRequestCtx("foo;", "http://stockcandles.de", nil, make(chan int), nil)
	assert.Error(t, err)
	request, err := httpService.CreateRequest(ctx)
	assert.Nil(t, request)
	assert.Error(t, err)
}
