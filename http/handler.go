package http

import (
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"time"

	"github.com/rs/zerolog/log"
)

type httpSrv struct {
	client *http.Client
}

func NewHttpService(timeout int64) HttpService {
	var httpService httpSrv
	httpService.client = &http.Client{Timeout: time.Duration(timeout) * time.Second}
	return &httpService
}

func (srv *httpSrv) CreateRequest(ctx RequestCtx) (*http.Request, error) {
	if len(ctx.method) == 0 {
		return nil, errors.New("invalid context given")
	}
	urlStr := ctx.Url
	var body io.Reader = ctx.Body
	request, err := http.NewRequest(ctx.method, urlStr, body)
	if err != nil {
		return nil, err
	}
	for header, value := range ctx.Header {
		request.Header.Add(header, value)
	}
	return request, nil
}

func (srv *httpSrv) DoRequest(request *http.Request) (*http.Response, error) {
	resp, err := srv.client.Do(request)
	if err != nil {
		return nil, err
	}
	log.Trace().Str("url", request.URL.String()).Interface("headers", resp.Header).Str("status", resp.Status).Msg("Response for url endpoint")
	return resp, err
}

func marshalJsonBody(jsonContent interface{}) ([]byte, error) {
	jsonBody, err := json.Marshal(jsonContent)
	if err != nil {
		return nil, err
	}
	return jsonBody, nil
}
