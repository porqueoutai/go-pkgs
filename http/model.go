package http

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"io"
	"net/http"
	"net/url"
	"sort"

	"github.com/rs/zerolog/log"
)

type HttpService interface {
	CreateRequest(ctx RequestCtx) (*http.Request, error)
	DoRequest(request *http.Request) (*http.Response, error)
}

type RequestCtx struct {
	method string
	Url    string
	Body   io.Reader
	Header map[string]string
}

func AssembleRequestCtx(method string, baseUrl string,
	queryParams map[string][]string,
	jsonContent interface{}, header map[string]string) (RequestCtx, error) {
	url, err := CreateURL(baseUrl, queryParams)
	if err != nil {
		return RequestCtx{}, err
	}
	var body io.Reader = nil
	if jsonContent != nil {
		jsonByteBody, err := marshalJsonBody(jsonContent)
		if err != nil {
			return RequestCtx{}, err
		}
		log.Debug().Str("body", string(jsonByteBody)).Str("url", url.String()).Msg("Body for url")
		body = bytes.NewBuffer(jsonByteBody)
	}
	return RequestCtx{method: method, Url: url.String(), Body: body, Header: header}, nil
}

func CreateURL(baseUrl string, queryParams map[string][]string) (*url.URL, error) {
	url, err := url.Parse(baseUrl)
	if err != nil {
		log.Error().Err(err).Str("url", baseUrl).Caller().Msg("Unable to parse url")
		return nil, err
	}
	query := url.Query()

	// sort the query headers here, we might sign the requests and must sustain a stable order
	keys := make([]string, 0, len(queryParams))
	for k := range queryParams {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	for _, key := range keys {
		values := queryParams[key]
		for _, value := range values {
			query.Add(key, value)
		}
	}
	url.RawQuery = query.Encode()
	return url, nil
}

func SignQueryParams(key string, queryParams map[string][]string) (string, error) {
	mac := hmac.New(sha256.New, []byte(key))
	url, _ := CreateURL("", queryParams)
	log.Trace().Str("param", url.RawQuery).Msg("Sign query param")
	_, err := io.WriteString(mac, url.RawQuery)
	if err != nil {
		return "", err
	}
	return hex.EncodeToString(mac.Sum(nil)), nil
}
